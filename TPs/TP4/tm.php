<!DOCTYPE html>
<html>
<head>
	<title>Table de multiplication</title>
	<link rel="stylesheet" type="text/css" href="tm.css">
</head>
<body>
<h1>Table de multiplication de 4</h1>
<pre>
<table>
	<tr>
		<th>x</th>
<?php
for($i=1;$i<10;$i++) echo "<th class='head'>$i</th>";
echo "</tr>";
for($a=1;$a<10;$a++){
	echo "<tr><td class='head'>$a</td>";
	for($i=1;$i<10;$i++){
		echo "<td>".$i*$a."</td>";
	}
	echo "</tr>";
}

echo "</pre>";

?>
</table>
</body>
</html>