<?php
    /* une ligne d'étoiles et passage à la ligne */
    echo "<pre>";
    for ($i=1 ; $i<=10 ; $i++) {
                 echo '*';
    }
    echo "</br>";

    /* un carré de 10 lignes */
    for ($j=1 ; $j<=10 ; $j++) {
        /* une ligne d'étoiles et passage à la ligne */
        for ($i=1 ; $i<=10 ; $i++) {
            echo '*';
        }
        echo "</br>";
    }
    echo "</br>";
    /* une table de multiplication de 10 lignes */
    for ($j=1 ; $j<=10 ; $j++) {

        for ($i=1 ; $i<=10 ; $i++) {
            echo $i*$j;
            echo ' ';
        }
        echo "</br>";

    }
    echo "</br>";
    /***** carré creux *****/

    /* une ligne d'étoiles et passage à la ligne */
    for ($i=1 ; $i<=10 ; $i++) {
        echo '*';
    }
    echo "</br>";

    /* les lignes creuses */
    for ($j=1 ; $j<=8 ; $j++) {
        /* une ligne creuse*/
        echo "*";
        for ($i=1 ; $i<=8 ; $i++) {
            echo ' ';
        }
        echo "*</br>";
    }

    /* une ligne d'étoiles et passage à la ligne */
    for ($i=1 ; $i<=10 ; $i++) {
        echo '*';
    }
    echo "</br>";
    echo "</pre>"

?>
