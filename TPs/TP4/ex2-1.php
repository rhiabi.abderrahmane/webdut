<?php
echo "<pre>";
// affiche les entiers de 1 à n
function compterJusquA ($n) {
    for ($i=1 ; $i<=$n ; $i++) {
        echo $i;
    }
}
// affiche n fois le caractère c
function repeter ($c,$n) {
    for ($i=1 ; $i<=$n ; $i++) {
        echo $c;
    }
}
// affiche une ligne de n fois le caractère c
function lignepleine ($c,$n) {
    repeter($c,$n);
    echo "\n";
}

// affiche une ligne creuse
function lignecreuse ($c1,$c2,$n) {
    echo "$c1";
    repeter($c2,$n-2);
    echo "$c1\n";
}

// affiche un carré creux
function carrecreux ($c1,$c2,$n) {
    lignepleine($c1,$n);
    for ($i=1 ; $i<=$n-2 ; $i++) {
        lignecreuse($c1,$c2,$n);
    }
    lignepleine($c1,$n);
}

// affiche un carré plein
function carreplein ($c,$n) {
  carrecreux($c,$c,$n);
}


// appels aux procédures définies ci-dessus
#compterJusquA(100);
#repeter('*',10);
#repeter('@',23);
#lignepleine('@',23);
#lignecreuse('o','x',6);
carrecreux('o','x',10);
carreplein('.',12);
echo "</pre>";

?>