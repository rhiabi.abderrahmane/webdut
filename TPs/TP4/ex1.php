<html>
 <head>
  <title>TPs PHP</title>
  <meta charset="utf-8">
  <link rel="stylesheet" type="text/css" href="ex1.css">
 </head>
 <body>

 <?php 
 
 function displayMap($n){
 	$colors=["Blue","Azure", "Beige", "BlueViolet", "Brown", "BurlyWood","Chartreuse", "CadetBlue", "Chocolate", "Coral", "Crimson", "Cyan", "DarkCyan", "DarkGreen", "Gold", "Grey", "Lime"] ;
 	echo "<h1>While loop</h1>";
	$i=1;
	echo "<div class='cont'>";
 	while($i<=$n) {
 		$c=$colors[rand(0,count($colors)-1)];
        echo "<div style='background-color: $c'>$i</div>"; 
        $i++;
    } 
    echo "</div>"; 	
}
displayMap(10000);
?> 
 </body>
</html>

